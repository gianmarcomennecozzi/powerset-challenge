# Technical Question: Powerset

### Problem

Suppose you're given a file with a single string of comma separated numbers like the following
```
$ cat input
123,456,789
```

Your program must print to STDOUT the powerset of the given set, like follows
(actual order is not relevant)

```
$ cat input | <your program> > output
$ cat output

123
456
789
123,456
123,789
456,789
123,456,789
```

Your solution must print to STDOUT and any extraneous output will be considered
an error (but you may print to STDERR at your convenience). Your program will be
tested against inputs of growing size, the more they can take, the better!

The solution must contain documentation explaining time and space complexity of
your implementation.

You should use only stuff other than the standard library of your language of
choice, any additional requirement should be documented and motivated. Java,
Python, Scala or Rust are preferred.

### Solution
```
$ cat input.txt | python3 main.py > output.txt
```

### Complexity
To solve this problem the backtracking approach has been used. \
Time complexity: O(N*2^N)\
Space complexity: O(N)\
where N is the number of elements in the input set

### Using docker

```
$ docker build -t powerset . 
$ docker run -i powerset < input.txt > output.txt
```