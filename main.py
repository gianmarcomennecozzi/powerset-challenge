def get_subsets(input_set, subset, i):
    """
    Print all the subset of the given input set
    :param input_set: given input set
    :param subset: helper set
    :param i: helper index
    :return: print subset
    """
    print(','.join(subset))
    for j in range(i, len(input_set)):
        subset.append(input_set[j])
        get_subsets(input_set, subset, j + 1)
        subset.pop()


def main():
    """
    Read from STDIN and get all the subsets of the given input set
    """
    input_set = input()
    input_set = input_set.split(',')
    subset = []
    get_subsets(input_set, subset, 0)


if __name__ == '__main__':
    main()
