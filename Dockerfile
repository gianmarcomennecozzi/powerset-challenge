FROM gcr.io/distroless/python3
WORKDIR /app
ADD main.py /app
CMD ["/app/main.py"]